#!/usr/bin/python3

"""
------------------------------------
Group: #4
İsmail Tüzün:       2036234
Yunus Emre Zengin:  2036408
------------------------------------
U2.py
=====
Usage: ./U2.py

This code runs servers and clients for U2. On U2 we created 2 servers and clients on different ports
in order to achieve running experiments concurrently. Consider T1 and U1 send message at a time to the U3,
Gateway going to forward them to the U2. In this case U2 can manage routing messages to the U3 without
blocking any of messages.
"""

import sys
import time
import socket
import socketserver
import threading

from ceng435utils import *

class UDPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our UDP server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):

        self.receivedData = self.request[0].strip()

        self.data = unzip(self.receivedData)

        print("{} wrote:".format(self.client_address[0]))
        print(self.data)
        # select the router and then send the packet to the router

        self.forward()
        # just send back the same data
        # self.request.sendall(zipMessage("OK-T2 {}".format(self.data["c"]), "GATEWAY"))

    def forward(self):
        host, port = serverInfo["U3"][self.sindex]
        host, port = routingTable[("U2", "U3")][self.sindex]

        # SOCK_DGRAM is the socket type to use for UDP sockets
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            # Connect to server and send data
            sock.sendto(zipMessageDict(self.data), (host, port))

            # Receive data from the server and shut down
            # received = str(sock.recv(1024), "utf-8")

        # print("Sent:     {}".format(self.receivedData))
        # print("Received: {}".format(received))

        # Send Back
        # unzipped = unzipStr(received)
        # unzipped["p"] += " - U2 - "
        # self.request[1].sendto(zipMessageDict(unzipped), self.client_address)

class UDP1Handler(UDPHandler):
    """
        This class actually only sets the sindex variable and
    sindex variable is used to chose one of the ports
    """
    sindex = 0

class UDP2Handler(UDPHandler):
    """
        This class actually only sets the sindex variable and
    sindex variable is used to chose one of the ports
    """
    sindex = 1

def createUDPserver1():
    """
    This function is used for create thread for UDP server.
    """
    host, port = serverInfo["U2"][0]

    UDPserver = socketserver.UDPServer((host, port), UDP2Handler)
    try:
        UDPserver.serve_forever()
    except KeyboardInterrupt:
        UDPserver.server_close()

def createUDPserver2():
    """
    This function is used for create thread for UDP server.
    """
    host, port = serverInfo["U2"][1]

    UDPserver = socketserver.UDPServer((host, port), UDP2Handler)
    try:
        UDPserver.serve_forever()
    except KeyboardInterrupt:
        UDPserver.server_close()


if __name__ == '__main__':
    if len(sys.argv) < 1:
        print("Usage: {} ".format(sys.argv[0]))
        sys.exit(-1)

    # Create treads for serving two UDP servers at the same time.
    threadUDP1 = threading.Thread(target=createUDPserver1)
    threadUDP2 = threading.Thread(target=createUDPserver2)
    threadUDP1.start()
    threadUDP2.start()

    threadUDP1.join()
    threadUDP2.join()
