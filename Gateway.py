#!/usr/bin/python3

"""
------------------------------------
Group: #4
İsmail Tüzün:       2036234
Yunus Emre Zengin:  2036408
------------------------------------
Gateway.py
=====
Usage: ./Gateway.py

This code runs servers and clients for Gateway. It's most important functionality is
forwarding message to the related router. This is implemented with forward functions within
TCPHandler and UDPHandler classes.

"""

import sys
import time
import socket
import socketserver
import threading

from ceng435utils import *


class TCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our TCP server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.

    """

    def handle(self):
        # self.request is the TCP socket connected to the client
        self.receivedData = self.request.recv(1024).strip()
        self.data = unzip(self.receivedData)

        print("{} wrote:".format(self.client_address[0]))
        print(self.data)

        # select the router and then send the packet to the router
        self.forward(self.data["d"][0], self.data)



    def forward(self, mode, data):
        """
        This function forwards the message from client T1 to related router (T2/U2).
        :param mode: selects next router by looking message destination
        :param data: the message
        :return: void
        """
        if (mode == "T"):
            host, port = routingTable[("GATEWAY", "T2")][0]
            # Create a socket (SOCK_STREAM means a TCP socket)
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                # Connect to server and send data
                sock.connect((host, port))
                sock.sendall(self.receivedData)

                # Receive data from the server and shut down
                received = str(sock.recv(1024), "utf-8")

            print("Sent:     {}".format(self.receivedData))
            print("Received: {}".format(received))

            # Send Back
            unzipped = unzipStr(received)
            unzipped["p"] += " - GATEWAY - "
            self.request.sendall(zipMessageDict(unzipped))

        else:
            host, port = routingTable[("GATEWAY", "U2")][1]

            # SOCK_DGRAM is the socket type to use for UDP sockets
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
                # Connect to server and send data
                sock.sendto(zipMessageDict(self.data), (host, port))

                # Receive data from the server and shut down
                received = str(sock.recv(1024), "utf-8")

            print("Sent:     {}".format(self.receivedData))
            print("Received: {}".format(received))

            # Send Back
            unzipped = unzipStr(received)
            unzipped["p"] += " - GATEWAY - "
            self.request.sendall(zipMessageDict(unzipped))





class UDPHandler(socketserver.BaseRequestHandler):
    """
    This class works similar to the TCP handler class, except that
    self.request consists of a pair of data and client socket, and since
    there is no connection the client address must be given explicitly
    when sending data back via sendto().
    """

    def handle(self):

        self.receivedData = self.request[0].strip()
        self.data = unzip(self.receivedData)

        print("{} wrote:".format(self.client_address[0]))
        print(self.data)
        # select the router and then send the packet to the router

        self.forward(self.data["d"][0], self.data)

    def forward(self, mode, data):
        """
        This function forwards the message from client U1 to related router (T2/U2).
        :param mode: selects next router by looking message destination
        :param data: the message
        :return: void
        """
        if (mode == "T"):
            host, port = routingTable[("GATEWAY", "T2")][0]

            # Create a socket (SOCK_STREAM means a TCP socket) since message should go to T3
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                # Connect to server and send data
                sock.connect((host, port))
                sock.sendall(self.receivedData)

                # Receive data from the server and shut down
                # received = str(sock.recv(1024), "utf-8")

            # We disabled send back procedure here, because we do not need in UDP experiments.
            # print("Sent:     {}".format(self.receivedData))
            # print("Received: {}".format(received))
            #
            # # Send Back
            # unzipped = unzipStr(received)
            # unzipped["p"] += " - GATEWAY - "
            # self.request[1].sendto(zipMessageDict(unzipped), self.client_address)


        else:
            host, port = routingTable[("GATEWAY", "U2")][1]

            # SOCK_DGRAM is the socket type to use for UDP sockets  ince message should go to U3
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
                # Connect to server and send data
                sock.sendto(zipMessageDict(self.data), (host, port))

                # Receive data from the server and shut down
                # received = str(sock.recv(1024), "utf-8")

            # We disabled send back procedure here, because we do not need in UDP experiments.

            # print("Sent:     {}".format(self.receivedData))
            # print("Received: {}".format(received))
            #
            # # Send Back
            # unzipped = unzipStr(received)
            # unzipped["p"] += " - GATEWAY - "
            # self.request[1].sendto(zipMessageDict(unzipped), self.client_address)

def createTCPserver():
    """
    This function is used for create thread for TCPserver.
    """
    host, port = serverInfo["GATEWAY"]

    TCPserver = socketserver.TCPServer((host, port), TCPHandler)
    try:
        TCPserver.serve_forever()
    except KeyboardInterrupt:
        TCPserver.server_close()

def createUDPserver():
    """
    This function is used for create thread for UDPserver.
    """
    host, port = serverInfo["GATEWAY"]

    UDPserver = socketserver.UDPServer((host, port), UDPHandler)
    try:
        UDPserver.serve_forever()
    except KeyboardInterrupt:
        UDPserver.server_close()


if __name__ == '__main__':
    if len(sys.argv) < 1:
        print("Usage: {} ".format(sys.argv[0]))
        sys.exit(-1)

    # Create treads for serving TCP and UDP servers at the same time.
    threadTCP = threading.Thread(target=createTCPserver)
    threadUDP = threading.Thread(target=createUDPserver)
    threadTCP.start()
    threadUDP.start()

    threadTCP.join()
    threadUDP.join()
