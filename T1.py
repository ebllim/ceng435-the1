#!/usr/bin/python3
"""
------------------------------------
Group: #4
İsmail Tüzün:       2036234
Yunus Emre Zengin:  2036408
------------------------------------
T1.py
=====
Usage: ./T1.py <destination>
    <destination> is in ["U3", "T3"]

This code connects to the gateway and sends our packege to the gateway using TCP.

"""



import sys
import time
import socket
import socketserver

from ceng435utils import *


def T1(dst):

    host, port = routingTable[("T1", "GATEWAY")]        # Get host and port values from routing table.
    message = zipMessage(getStrDatetime(), dst)         # Put the message in the pre-determined form.

    # Create a socket (SOCK_STREAM means a TCP socket)
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        # Connect to server and send data
        sock.connect((host, port))
        sock.sendall(message)

        # Receive data from the server and shut down
        received = str(sock.recv(1024), "utf-8")

    print("Sent:     {}".format(message))
    print("Received: {}".format(received))




if __name__ == '__main__':
    if len(sys.argv) < 2 :
        print("Usage: {} <destination>".format(sys.argv[0]))
        sys.exit(-1)
    dst = sys.argv[1].upper()
    if dst not in ["T1", "T2", "T3", "U1", "U2", "U3", "GATEWAY"]:
        print("Destination is not given correctly, given: {}".format(sys.argv[1]))
        sys.exit(-2)
    T1(dst)
