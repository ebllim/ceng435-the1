#!/usr/bin/python3

"""
------------------------------------
Group: #4
İsmail Tüzün:       2036234
Yunus Emre Zengin:  2036408
------------------------------------
U3.py
=====
Usage: ./U3.py

This code runs servers for U3. On U3 we created 2 servers on different ports
in order to achieve running experiments concurrently. Consider T1 and U1 send message at a time to the U3,
Gateway going to forward them to the U2 and U2 would try to forward the messages to U3. In this case U3 can
recieve both messages concurrently from diffent ports.

"""

import sys
import time
import socket
import socketserver
import threading

from ceng435utils import *

class UDPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our UDP server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        self.receivedData = self.request[0].strip()
        receiveTime = getDatetime()
        self.data = unzip(self.receivedData)

        print("{} wrote: {}".format(self.client_address[0], self.data))
        print("Time Delay: {}".format(getDiffTimes(receiveTime, strToTime(self.data["c"]))))

        # Send Back
        # self.data["p"] += " - U3 - "
        # self.request[1].sendto(zipMessageDict(self.data), self.client_address)


def createUDPserver1():
    """
    This function is used for create thread for UDPserver.
    """
    host, port = serverInfo["U3"][0]

    UDPserver = socketserver.UDPServer((host, port), UDPHandler)
    UDPserver.sindex = 0
    try:
        UDPserver.serve_forever()
    except KeyboardInterrupt:
        UDPserver.server_close()

def createUDPserver2():
    """
    This function is used for create thread for UDPserver.
    """
    host, port = serverInfo["U3"][1]

    UDPserver = socketserver.UDPServer((host, port), UDPHandler)
    UDPserver.sindex = 1
    try:
        UDPserver.serve_forever()
    except KeyboardInterrupt:
        UDPserver.server_close()


if __name__ == '__main__':
    if len(sys.argv) < 1:
        print("Usage: {} ".format(sys.argv[0]))
        sys.exit(-1)

    # Create treads for serving two UDP servers at the same time.
    threadUDP1 = threading.Thread(target=createUDPserver1)
    threadUDP2 = threading.Thread(target=createUDPserver2)
    threadUDP1.start()
    threadUDP2.start()

    threadUDP1.join()
    threadUDP2.join()
