#!/usr/bin/python3

"""
------------------------------------
Group: #4
İsmail Tüzün:       2036234
Yunus Emre Zengin:  2036408
------------------------------------
T2.py
=====
Usage: ./T2.py

This code runs servers and clients for T2. On T2 we created 2 servers and clients on different ports
in order to achieve running experiments concurrently. Consider T1 and U1 send message at a time to the T3,
Gateway going to forward them to the T2. In this case T2 can manage routing messages to the T3 without
blocking any of messages.

"""

import sys
import time
import socket
import socketserver
import threading

from ceng435utils import *

class TCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our TCP server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        self.receivedData = self.request.recv(1024).strip()
        self.data = unzip(self.receivedData)

        print("{} wrote:".format(self.client_address[0]))
        print(self.data)

        # Forward the message to T3
        self.forward()
        # self.request.sendall(zipMessage("OK-T2 {}".format(self.data["c"]), "GATEWAY"))

    def forward(self):
        """
        This function forwards the message from T2 to T3.
        """

        host, port = routingTable[("T2", "T3")][self.sindex]

        # Create a socket (SOCK_STREAM means a TCP socket)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            # Connect to server and send data
            sock.connect((host, port))
            sock.sendall(self.receivedData)

            # Receive data from the server and shut down
            received = str(sock.recv(1024), "utf-8")

        print("Sent:     {}".format(self.receivedData))
        print("Received: {}".format(received))
        # print("Sindex was : {}".format(self.sindex))

        # Send Back
        unzipped = unzipStr(received)
        unzipped["p"] += " - T2 - "
        self.request.sendall(zipMessageDict(unzipped))


class TCP1Handler(TCPHandler):
    """
        This class actually only sets the sindex variable and
    sindex variable is used to chose one of the ports
    """
    sindex = 0


class TCP2Handler(TCPHandler):
    """
        This class actually only sets the sindex variable and
    sindex variable is used to chose one of the ports
    """
    sindex = 1

def createTCPserver1():
    """
    This function is used for create thread for TCPserver.
    """
    host, port = serverInfo["T2"][0]

    TCPserver = socketserver.TCPServer((host, port), TCP2Handler)
    try:
        TCPserver.serve_forever()
    except KeyboardInterrupt:
        TCPserver.server_close()

def createTCPserver2():
    """
    This function is used for create thread for TCPserver.
    """
    host, port = serverInfo["T2"][1]

    TCPserver = socketserver.TCPServer((host, port), TCP2Handler)
    try:
        TCPserver.serve_forever()
    except KeyboardInterrupt:
        TCPserver.server_close()


if __name__ == '__main__':
    if len(sys.argv) < 1:
        print("Usage: {} ".format(sys.argv[0]))
        sys.exit(-1)

    # Create treads for serving two TCP servers at the same time.
    threadTCP1 = threading.Thread(target=createTCPserver1)
    threadTCP2 = threading.Thread(target=createTCPserver2)
    threadTCP1.start()
    threadTCP2.start()

    threadTCP1.join()
    threadTCP2.join()
