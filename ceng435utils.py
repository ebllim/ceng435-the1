"""
------------------------------------
Group: #4
İsmail Tüzün:       2036234
Yunus Emre Zengin:  2036408
------------------------------------
ceng435utils.py
=====
from ceng435utils import *

This code contains utility functions

"""

import sys
import ast
from datetime import datetime

#################### String-dict manupulation group #########################
# this groups of functions are providing dict
# to string and string to dict conversions
# for us since we are sending our messages as
# dictionary string

def zipMessage(content, dst):
    message = {"p":""}
    message["c"] = content
    message["d"] = dst
    return str(message).encode()

def zipMessageDict(d):
    return str(d).encode()

def unzip(message):
    return ast.literal_eval((message.decode()))

def unzipStr(message):
    return ast.literal_eval(message)
#################### String-dict manupulation group #########################


# serverInfo = {
#     "GATEWAY": ("10.10.2.1", 10000),
#     "T2": [("10.10.3.2", 11001), ("10.10.5.1", 11002)],
#     "T3": [("10.10.5.2", 12001), ("10.10.5.2", 12002)],
#     "U2": [("10.10.4.2", 11001), ("10.10.6.1", 11002)],
#     "U3": [("10.10.6.2", 12001), ("10.10.6.2", 12002)]
# }

#################### String-time manupulation group #########################
# is this function group we managed time related issues
# and time <-> string conversions
def getDatetime():
    return datetime.now()
def timeToStr(d):
    return d.strftime("%Y-%m-%d %H:%M:%S.%f")
def strToTime(s):
    return datetime.strptime(s, "%Y-%m-%d %H:%M:%S.%f")
def getStrDatetime():
    return timeToStr(datetime.now())
def getDiffTimes(t1, t2):
    d = t1 - t2
    return d.total_seconds()
#################### String-time manupulation group #########################

# Since hardcoding all ip and ports while creating sockets going to kill our code portability
# so we created this dictionary to solve this issue
routingTable = {
    ("T1", "GATEWAY"): ("10.10.1.2", 10000),
    ("U1", "GATEWAY"): ("10.10.2.2", 10000),

    ("GATEWAY", "T2"): [("10.10.3.2", 11001), ("10.10.3.2", 11002)],
    ("GATEWAY", "U2"): [("10.10.4.2", 11001), ("10.10.4.2", 11002)],

    ("T2", "T3"): [("10.10.5.2", 12001), ("10.10.5.2", 12002)],
    ("U2", "U3"): [("10.10.6.2", 12002), ("10.10.6.2", 12002)]
}

serverInfo = {
    "GATEWAY": ("0.0.0.0", 10000),
    "T2": [("0.0.0.0", 11001), ("0.0.0.0", 11002)],
    "T3": [("0.0.0.0", 12001), ("0.0.0.0", 12002)],
    "U2": [("0.0.0.0", 11001), ("0.0.0.0", 11002)],
    "U3": [("0.0.0.0", 12001), ("0.0.0.0", 12002)]
}


# At initial attempts we tested our code in local host so the code below was written for
# our local host
# table = [
#     ["GATEWAY", "T1", "0.0.0.0", 10000],    #
#     ["GATEWAY", "U1", "0.0.0.0", 10001],    #
#     ["T2", "GATEWAY", "0.0.0.0", 10010],    # GATEWAY=>T2
#     ["T2", "T1", "0.0.0.0", 10000],         # T1=>GATEWAY->T2
#     ["T3", "T1", "0.0.0.0", 10000],         # T1=>GATEWAY->T2->T3
#     ["T3", "T2", "0.0.0.0", 10000],         # T2=>T3
#     # ["T1", "T2", "0.0.0.0", 10000],         # T2=>GATEWAY->T1
#     ["GATEWAY", "T2", "0.0.0.0", 10000],         # T2=>T3
#     ["U2", "GATEWAY", "0.0.0.0", 10011],    #
#     #
#     # ["T1", "GATEWAY", "0.0.0.0", 10000],
#     # ["T1", "T3", "0.0.0.0", 12000],
#     # ["U1", "GATEWAY", "0.0.0.0", 10001],
#     # ["GATEWAY", "T2", "10.10.2.2", 8080],
#     # ["GATEWAY", "U2", "10.10.3.1", 8080],
#     # ["T2", "T3", "10.10.2.2", 8080],
#     # ["U2", "U3", "10.10.3.1", 8080]
# ]
# def getHostPort(src, dst):
#     for te in table:
#         if( te[0] == src and te[1] == dst):
#             return (te[2], te[3])
#     print("nextNode couldn't find")
#     sys.exit(404)
