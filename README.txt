Group: #4
------------
İsmail Tüzün
2036234
------------
Yunus Emre Zengin
2036408

################################################################# Running The System ################################################################
    We created a git project for the homework and we created one scritp for each computer in our slice. By this git project we could make changes and pull
    the project from all computers. Our scripts for computers are:
	
    Gateway	: Gateway.py
    T1 	: T1.py
    T2 	: T2.py
    T3 	: T3.py
    U1 	: U1.py
    U2 	: U2.py
    U3 	: U3.py
	
	ceng435utils.py is used by all scripts listed above.
	
    In order to run the system you should run following scripts in related computers:
    ./Gateway.py 	on gateway
    ./T2.py		on t2
    ./T3.py		on t3
    ./U2.py		on u2
    ./U3.py		on u3

    After running above scripts we are ready to send messages to U3 or T3 from U1 or T1.

    To send message from T1 you must run:
    ./T1 T3
    ./T1 U3
    commands.

    To send message from T1 you must run:
    ./U1 T3
    ./U1 U3
    commands.
################################################################# Running The System ################################################################
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
################################################################# Time Syncronizing ################################################################
    Since we are going to measure end-to-end delay we needed to syncronize computers. To do so we used ntpdate utility. Firstly we set our gateway as
    ntp server. Then we tried to synchronize all computers according to gateway. Referance link: https://knowm.org/how-to-synchronize-time-across-a-linux-cluster/

    > Do this for all nodes. This is beacuse the nodes should have same timezone settings.
    `sudo dpkg-reconfigure tzdata`
    pick None of the above -> UTC
    > At gateway, if port 123 is not open, we cannot use it as ntp-server.
    `sudo nmap -sU -p 123 timeserver`

    But we realized that synchronization is not perfect and offsets are not always small enough. So we repeated synchronization process until we achieve
    reasonable offsets. However, this repeating process can be boring so we created a python script that called "adjust.py".This file exists in all computers
    in slice. You can run it with:
        ./adjust.py <gateway eth0 ip>
    and script runs until achieve offset that smalled 1ms.
################################################################# Time Syncronizing ################################################################
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
################################################################## Network Emulation ################################################################
    For all experiments adding emulation delay to all links is needed. 30ms, 60ms delays are going to be added to all links with +-5ms normal distribution.
    To adding 30ms delay for all links you need to run these commands:

        _________________________________gateway__________________________________
        sudo tc qdisc change dev eth1 root netem delay 30ms 5ms distribution normal // you must use "add" keyword instead of "change" if you are adding delay first time
        sudo tc qdisc change dev eth2 root netem delay 30ms 5ms distribution normal
        sudo tc qdisc change dev eth3 root netem delay 30ms 5ms distribution normal
        sudo tc qdisc change dev eth4 root netem delay 30ms 5ms distribution normal

        ____________________________________T1____________________________________
        sudo tc qdisc change dev eth1 root netem delay 30ms 5ms distribution normal

        ____________________________________T2____________________________________
        sudo tc qdisc change dev eth1 root netem delay 30ms 5ms distribution normal
        sudo tc qdisc change dev eth2 root netem delay 30ms 5ms distribution normal

        ____________________________________T3____________________________________
        sudo tc qdisc change dev eth1 root netem delay 30ms 5ms distribution normal

        ____________________________________U1____________________________________
        sudo tc qdisc change dev eth1 root netem delay 30ms 5ms distribution normal

        ____________________________________U2____________________________________
        sudo tc qdisc change dev eth1 root netem delay 30ms 5ms distribution normal
        sudo tc qdisc change dev eth2 root netem delay 30ms 5ms distribution normal

        ____________________________________U3____________________________________
        sudo tc qdisc change dev eth1 root netem delay 30ms 5ms distribution normal

    For experiments 3 and 4 we need to add packet loss probability to all links with 1ms delay. 0.1 or 5 percent loss probability must be added to all links
    To adding 5% loss probablity with 1ms for all links you need to run these commands:

        _________________________________gateway__________________________________
        sudo tc qdisc change dev eth1 root netem  loss 5% delay 1ms
        sudo tc qdisc change dev eth2 root netem  loss 5% delay 1ms
        sudo tc qdisc change dev eth3 root netem  loss 5% delay 1ms
        sudo tc qdisc change dev eth4 root netem  loss 5% delay 1ms

        ____________________________________T1____________________________________
        sudo tc qdisc change dev eth1 root netem  loss 5% delay 1ms

        ____________________________________T2____________________________________
        sudo tc qdisc change dev eth1 root netem  loss 5% delay 1ms
        sudo tc qdisc change dev eth2 root netem  loss 5% delay 1ms

        ____________________________________T3____________________________________
        sudo tc qdisc change dev eth1 root netem  loss 5% delay 1ms

        ____________________________________U1____________________________________
        sudo tc qdisc change dev eth1 root netem  loss 5% delay 1ms

        ____________________________________U2____________________________________
        sudo tc qdisc change dev eth1 root netem  loss 5% delay 1ms
        sudo tc qdisc change dev eth2 root netem  loss 5% delay 1ms

        ____________________________________U3____________________________________
        sudo tc qdisc change dev eth1 root netem  loss 5% delay 1ms

################################################################## Network Emulation ################################################################
