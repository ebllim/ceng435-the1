#!/usr/bin/python3

import subprocess
import sys

def main():
    ntp_server_ip = sys.argv[1]
    stop_result = subprocess.run(["sudo", "service", "ntp", "stop"], stdout=subprocess.PIPE)
    find_server = subprocess.run(["sudo", "ntpdate", "-q", ntp_server_ip], stdout=subprocess.PIPE)
    result = subprocess.run(["sudo", "ntpdate", ntp_server_ip], stdout=subprocess.PIPE)
    result = result.stdout.decode().strip('\n sec').split('offset ')[1]
    result = float(result)

    while abs(result) > 0.0009:
        result = subprocess.run(["sudo", "ntpdate", ntp_server_ip], stdout=subprocess.PIPE)
        print(result.stdout.decode())
        result = result.stdout.decode().strip('\n sec').split('offset ')[1]
        print("delay:", result)
        result = float(result)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: {} <ntp_server_ip>".format(sys.argv[0]))
        sys.exit(-1)
    main()

