#!/usr/bin/python3

"""
------------------------------------
Group: #4
İsmail Tüzün:       2036234
Yunus Emre Zengin:  2036408
------------------------------------
T3.py
=====
Usage: ./T3.py

This code runs servers for T3. On T3 we created 2 servers on different ports
in order to achieve running experiments concurrently. Consider T1 and U1 send message at a time to the T3,
Gateway going to forward them to the T2 and T2 would try to forwar the messages to T3. In this case T3 can
recieve both messages concurrently from diffent ports.

"""

import sys
import time
import socket
import socketserver
import threading

from ceng435utils import *

class TCPHandler(socketserver.BaseRequestHandler):
    """
    The request handler class for our TCP server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        # getting the message
        self.receivedData = self.request.recv(1024).strip()
        receiveTime = getDatetime()
        self.data = unzip(self.receivedData)


        print("{} wrote: {}".format(self.client_address[0], self.data))
        print("Time Delay: {}".format(getDiffTimes(receiveTime, strToTime(self.data["c"]))))
        # select the router and then send the packet to the router

        # Send Back
        self.data["p"] += " - T3 - "
        self.request.sendall(zipMessageDict(self.data))


def createTCPserver1():
    """
    This function is used for create thread for TCPserver.
    """
    host, port = serverInfo["T3"][0]

    TCPserver = socketserver.TCPServer((host, port), TCPHandler)
    TCPserver.sindex = 0
    try:
        TCPserver.serve_forever()
    except KeyboardInterrupt:
        TCPserver.server_close()

def createTCPserver2():
    """
    This function is used for create thread for TCPserver.
    """
    host, port = serverInfo["T3"][1]

    TCPserver = socketserver.TCPServer((host, port), TCPHandler)
    TCPserver.sindex = 1
    try:
        TCPserver.serve_forever()
    except KeyboardInterrupt:
        TCPserver.server_close()


if __name__ == '__main__':
    if len(sys.argv) < 1:
        print("Usage: {} ".format(sys.argv[0]))
        sys.exit(-1)

    # Create treads for serving 2 TCP servers at the same time.
    threadTCP1 = threading.Thread(target=createTCPserver1)
    threadTCP2 = threading.Thread(target=createTCPserver2)
    threadTCP1.start()
    threadTCP2.start()

    threadTCP1.join()
    threadTCP2.join()
