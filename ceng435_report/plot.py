import matplotlib.pyplot as plt
import numpy as np


def line_func(p1, p2):
    m = (p1[1]-p2[1])/(p1[0]-p2[0])
    c = p2[1] - m*p2[0]
    return (m,c)

# exp1 - 30ms 281.64435
# exp1 - 60ms 538.22481
p1 = (30, 281.64435, 3.15)
p2 = (60, 538.22481, 2.75)

# exp2 - 30ms 158.60418
# exp2 - 60ms 312.22304
# p1 = (30, 158.60418, 3.25)
# p2 = (60, 312.22304, 1.95)

x = np.linspace(0, 70)
m,c = line_func(p1,p2)
y = x*m+c
a = [p1[0],p2[0]]
b = [p1[1],p2[1]]

plt.title("End-to-end delay vs Emulation delay (Exp 2)")
plt.xticks(np.arange(min(x), max(x)+1, 5))
plt.yticks(np.arange(min(y), max(y), 50))
plt.grid(color='r', linestyle='-.', linewidth=0.2)

# plt.plot(a, b, 'ro')
plt.plot(x,y)

plt.errorbar(a[0], b[0], xerr=5, yerr=p1[2], color='r') # this yerr value (confidence interval) is calculated by wolphram alpha
plt.errorbar(a[1], b[1], xerr=5, yerr=p2[2] ,color='r') # this yerr value (confidence interval) is calculated by wolphram alpha

x1,x2,y1,y2 = plt.axis()
plt.axis((x1,x2,0,y2))
plt.ylabel('end-to-end delay(ms)')
plt.xlabel('emulation delay(ms)')
plt.show()
